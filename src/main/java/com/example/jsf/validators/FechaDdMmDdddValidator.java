package com.example.jsf.validators;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator(value = "fechaddMMYYYY")
public class FechaDdMmDdddValidator implements Validator {

	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {

		Date date = null;
		try {
			date = new Date();
//			String fechaField = (String) value;
//			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
//			date = sdf.parse(fechaField);
//			String d = sdf.format(date);
//			
//			if (!fechaField.equals(sdf.format(date))) {
//				date = null;
//			}
		} catch (Exception ex) {
			date = null;
		}

		if (date == null) {
			FacesMessage message = new FacesMessage();
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			message.setSummary("Validator Formato de fecha no valido (dd/mm/yyyy)");

			throw new ValidatorException(message);
		}

	}

}
