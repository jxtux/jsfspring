package com.example.jsf.convert;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

@FacesConverter("com.example.jsf.convert.DateDdMmYyyyConvert")
public class DateDdMmYyyyConvert implements Converter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {

		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			
			return sdf.parse(value);			
		} catch (Exception ex) {
			FacesMessage message = new FacesMessage();
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			message.setSummary("Convert Error de formato de fecha (dd/mm/yyyy)");

			throw new ConverterException(message);
		}

	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {

		return value.toString();
	}

}
