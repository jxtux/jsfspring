package com.example.jsf.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;
import com.example.jsf.entity.Employees;
import com.example.jsf.entity.Jobs;
import com.example.jsf.repository.EmployeesRepository;

@Named(value="empleadoController")
@RequestScoped
//@ViewScoped
public class EmpleadoController {

	@Autowired
	private EmployeesRepository employeesRepository;

	private Employees empl = new Employees();

	private Jobs job = new Jobs();

	private List<Employees> list;

	@PostConstruct
	public void init() {
		list = employeesRepository.listOrdenarId();
	}

	public void salvar() {
		empl.setJobId(job);
		Employees e = employeesRepository.save(empl);
		
		if (e.getEmployeeId() != null) {
			list = employeesRepository.listOrdenarId();
		}

		empl = new Employees();
		job = new Jobs();
	}

	public void eliminar(Integer id) {
		Employees e = employeesRepository.findOne(id);
		employeesRepository.delete(e);

		list = employeesRepository.listOrdenarId();
	}

	public void cancelar() {
		empl = new Employees();
		job = new Jobs();
	}

	public void editar(Integer id) {
		Employees e = employeesRepository.findOne(id);
		setEmpl(e);
		setJob(e.getJobId());

		list = employeesRepository.listOrdenarId();
	}

	public Employees getEmpl() {
		return empl;
	}

	public void setEmpl(Employees empl) {
		this.empl = empl;
	}

	public Jobs getJob() {
		return job;
	}

	public void setJob(Jobs job) {
		this.job = job;
	}

	public List<Employees> getList() {
		return list;
	}

	public void setList(List<Employees> list) {
		this.list = list;
	}
	
	public String moveToSaludo() {
		
		return "/page/saludo";
	}

}
