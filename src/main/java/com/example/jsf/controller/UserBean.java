package com.example.jsf.controller;

import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;
import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.jsf.entity.Users;
import com.example.jsf.repository.EmployeesRepository;
import com.example.jsf.repository.UsersRepository;

@Named(value = "userBean")
@RequestScoped
public class UserBean {

	@Autowired
	private UsersRepository usersRepository;

	private Users user = new Users();

	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}

	public String verificarDatos() throws Exception {
		String resultado;
		try {
			Users u = usersRepository.searchUser(user.getUsername(), user.getPassword());

			if (u != null) {
				// Guardamos la session
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("usuario", u);

				resultado = "/empleado/admin";
			} else {
				resultado = "/common/error";
			}
		} catch (Exception e) {
			throw e;
		}

		return resultado;
	}

	public boolean verificarSession() {

		if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuario") == null) {
			return false;
		}

		return true;
	}

	public void verificarSessionLoad(ComponentSystemEvent event) {

		if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuario") == null) {
			FacesContext fc = FacesContext.getCurrentInstance();
			ConfigurableNavigationHandler nav = (ConfigurableNavigationHandler) fc.getApplication()
					.getNavigationHandler();

			nav.performNavigation("/common/error");
		}

	}

	public String cerrarSession() {
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();

		return "/login/login?faces-redirect=true";
	}

}
