package com.example.jsf.controller;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.jsf.entity.Jobs;
import com.example.jsf.repository.JobsRepository;
import com.example.jsf.resource.ResourcesUtil;

@Named(value="nameController")
@ViewScoped
public class NameController {

	private String name;

	private String name2;
	
	private String dato;

	public Date date;
	
	private String job;
	
	private List<Jobs> jobs;
	
	@Autowired
	private JobsRepository jobsRepository;

	@PostConstruct
	public void init() {
		jobs = jobsRepository.listJobOrderTitle();
	}
	
	public String saludar() {
		if(name2 == null) {
			return "";
		}
		
		return ResourcesUtil.getString("#{msg['app.saludo']}" + name2);
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

//	public String getSaludo() {
//		return saludo;
//	}
//
//	public void setSaludo(String saludo) {
//		this.saludo = saludo;
//	}

	public String getName2() {
		return name2;
	}

	public void setName2(String name2) {
		this.name2 = name2;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getDato() {
		return dato;
	}

	public void setDato(String dato) {
		this.dato = dato;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public List<Jobs> getJobs() {
		return jobs;
	}

	public void setJobs(List<Jobs> jobs) {
		this.jobs = jobs;
	}

}
