package com.example.jsf.controller;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import com.example.jsf.bean.Persona;

@Named(value = "login")
@ViewScoped
public class loginController {

	private Persona persona = new Persona();

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public void validarSexo(FacesContext context, UIComponent toValidate, Object value) {

		context = FacesContext.getCurrentInstance();
		String texto = (String) value;

		if (!texto.equalsIgnoreCase("M") && !texto.equalsIgnoreCase("F")) {
			((UIInput) toValidate).setValid(false);
			context.addMessage(toValidate.getClientId(context), new FacesMessage("Sexo No Valido"));
		}
	}

}
