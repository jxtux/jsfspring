package com.example.jsf.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.inject.Named;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import com.example.jsf.entity.Employees;
import com.example.jsf.entity.Jobs;
import com.example.jsf.repository.EmployeesRepository;
import com.example.jsf.repository.JobsRepository;

import lombok.Getter;
import lombok.Setter;

@Named(value = "empleadoBean")
@RequestScoped
public class EmpleadoBean {

	@Autowired
	private EmployeesRepository employeesRepository;

	@Autowired
	private JobsRepository jobsRepository;

	@Getter
	@Setter
	private Employees empleado = new Employees();

	@Getter
	@Setter
	private Jobs job = new Jobs();

	@Getter
	@Setter
	private List<Employees> employees;

	@Getter
	@Setter
	private List<Jobs> jobs;

	@PostConstruct
	public void init() {
		employees = employeesRepository.listOrdenarId();
		jobs = jobsRepository.listJobOrderTitle();
	}

	public void save() {
		empleado.setJobId(job);
		Employees e = employeesRepository.save(empleado);

		if (e != null) {
			employees = employeesRepository.listOrdenarId();
			empleado = new Employees();
			job = new Jobs();
		}
	}

}
