package com.example.jsf.controller;

import javax.faces.bean.RequestScoped;
import javax.inject.Named;

import com.example.jsf.entity.Employees;

@Named(value = "resultadoBean")
@RequestScoped
public class ResultadoBean {
	
	private Employees empleadoRe = new Employees();

	public Employees getEmpleadoRe() {
		return empleadoRe;
	}

	public void setEmpleadoRe(Employees empleadoRe) {
		this.empleadoRe = empleadoRe;
	}

}
