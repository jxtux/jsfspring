package com.example.jsf.controller;

import java.util.Date;

import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.inject.Named;

import lombok.Getter;
import lombok.Setter;

@Named(value="conVal")
@SessionScoped
public class ConvertValidation {
	
	@Getter 
	@Setter
	Date date;
	
	@Getter 
	@Setter
	double amount;
	
	@Getter 
	@Setter
	String username;
	
	@Getter 
	@Setter
	int age;
	
	@Getter 
	@Setter
	double salary;	

	@Getter 
	@Setter
	String password;


}
