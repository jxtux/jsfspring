package com.example.jsf.repository;

import com.example.jsf.entity.Jobs;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface JobsRepository extends JpaRepository<Jobs, String> {

    Jobs findByJobId(@Param("jobId") String jobId);
    
    @Query(value = "select new Jobs(j.jobId, j.jobTitle) from Jobs j order by j.jobTitle asc")
    List<Jobs> listJobOrderTitle();

}
