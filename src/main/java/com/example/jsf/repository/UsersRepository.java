package com.example.jsf.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.jsf.entity.Users;

@Repository
public interface UsersRepository extends JpaRepository<Users,Integer> {
	
	@Query(value = "select new Users(u.userid) from Users u where u.username = :username and u.password = :password")
    Users searchUser(@Param("username") String username,@Param("password") String password);    

}
