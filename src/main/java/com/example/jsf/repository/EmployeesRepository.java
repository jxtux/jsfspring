package com.example.jsf.repository;

import com.example.jsf.entity.Employees;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeesRepository extends JpaRepository<Employees,Integer> {

    @Query(value = "select e "
            + " from Employees e where e.jobId.jobId = :jobId")
    List<Employees> findJobId(@Param("jobId") String jobId);
    

    @Query(value = "select e from Employees e order by e.employeeId desc")
    List<Employees> listOrdenarId();
    
    Employees findByEmail(@Param("email") String email);
}
