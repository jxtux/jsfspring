package com.example.jsf;

import static org.junit.Assert.assertEquals;

import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.jsf.entity.Employees;
import com.example.jsf.entity.Jobs;
import com.example.jsf.entity.Users;
import com.example.jsf.repository.EmployeesRepository;
import com.example.jsf.repository.JobsRepository;
import com.example.jsf.repository.UsersRepository;


@RunWith(SpringRunner.class)
@SpringBootTest
public class JsfSpringApplicationTests {

	@Autowired
    EmployeesRepository empleesRepository;

    @Autowired
    JobsRepository jobsRepository;
    
    @Autowired
    UsersRepository usersRepository;

//    @Test
    public void saveEmployee() {
        Employees e = new Employees(99999, "HUIN", "cero.one.9999x@gmail.com", new Date());
        Jobs j = jobsRepository.findByJobId("IT_PROG");
        e.setJobId(j);
        Employees e1 = empleesRepository.save(e);

        Assert.assertNotNull(e1.getEmployeeId());
    }
    
//    @Test
    public void findJobs() {
        List<Jobs> list = jobsRepository.listJobOrderTitle();
        for (Jobs j : list) {
            System.out.println(j.getJobId()+": "+j.getJobTitle());
        }
    }
    
//    @Test
    public void findEmployees() {
        List<Employees> list = empleesRepository.findJobId("IT_PROG");
        for (Employees e : list) {
            System.out.println(e.getFirstName());
        }
    }
    
//    @Test
    public void updateEmployees() {
        Employees es = empleesRepository.findOne(999999);
        es.setFirstName("Joan 30x");
        es.setHireDate(new Date());
        
        Employees eSave = empleesRepository.save(es);
        assertEquals(eSave.getFirstName(),es.getFirstName());
    }
    
    @Test
    public void findUser() {
        Users u = usersRepository.searchUser("alexis", "123456");
        
        Assert.assertNotNull(u.getUserid());
    }
}
